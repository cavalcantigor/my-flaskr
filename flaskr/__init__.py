from flask import Flask
from flask_migrate import Migrate
from .models.db import db
from .config import Config, DevConfig, HerokuConfig
import os


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists
        app.config.from_object(Config)
        env = os.environ.get('FLASK_CONFIG', "")
        if env == "Heroku":
            app.config.from_object(HerokuConfig)
        elif env == "Dev":
            app.config.from_object(DevConfig)
        else:
            pass
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')

    from .models import comment_model, post_model, user_model

    db.init_app(app)
    migrate = Migrate(app, db)

    return app
