from flask import (
    Blueprint, flash, redirect, render_template, request, url_for, session
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from .controllers.post_controller import PostController
from .controllers.comment_controller import CommentController

bp = Blueprint('blog', __name__)


@bp.route('/', methods=('GET', ))
def index():
    posts = PostController.get_posts()
    return render_template('blog/index.html', posts=posts)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if not body:
            error = 'Body is required.'

        if error is not None:
            flash(error)
        else:
            PostController.create_post(title, body, session.get('user_id'))
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')


def get_post(post_id, check_author=True):
    post = PostController.get_posts(post_id=post_id)

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(post_id))

    if check_author and post.author_id != session.get('user_id'):
        abort(403)

    return post


@bp.route('/<int:post_id>/update', methods=('GET', 'POST'))
@login_required
def update(post_id):
    post = get_post(post_id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'
        elif not body:
            error = 'Body is required.'

        if error is not None:
            flash(error)
        else:
            if PostController.update_post(title, body):
                return redirect(url_for('blog.index'))
            else:
                error = 'Was not possible update the post.'
                flash(error)

    return render_template('blog/update.html', post=post)


@bp.route('/<int:post_id>/delete', methods=('POST',))
@login_required
def delete(post_id):
    if PostController.delete_post(post_id):
        return redirect(url_for('blog.index'))
    else:
        error = 'Was not possible to delete the post.'
        flash(error)


@bp.route('/<int:post_id>/details', methods=('GET',))
def details(post_id):
    post = get_post(post_id, check_author=False)
    comments = get_comments(post_id)

    if post is None:
        error = 'Post not found.'
        flash(error)

    return render_template('blog/details.html', post=post, comments=comments)


def get_comments(post_id):
    comments = CommentController.get_comments_post(post_id)

    if comments is None:
        abort(500, 'Erro interno.')

    return comments


@bp.route('/<int:post_id>/comment', methods=('POST', ))
@login_required
def comment(post_id):
    text_comment = request.form['comment']
    author_id = session.get('user_id')

    if CommentController.create_comment(text_comment, author_id, post_id):
        return redirect(url_for('blog.details', post_id=post_id))
    else:
        abort(500, 'Erro interno.')


@bp.route('/search', methods=('GET', ))
def search():
    search_term = request.args['search']
    posts = PostController.search_posts(search_term)

    if posts is None:
        abort(500, 'Erro interno.')

    return render_template('blog/search.html', posts=posts, search=search)
