from ..models.post_model import PostModel
from ..models.db import db
from datetime import datetime


class PostController:

    @staticmethod
    def get_posts(post_id=None):
        if post_id is None:
            return PostModel.query.all()
        else:
            return PostModel.query.filter(PostModel.id == post_id).first()

    @staticmethod
    def create_post(title, body, author_id):
        new_post = PostModel(title=title, body=body,
                             author_id=author_id, created=datetime.now())
        db.session.add(new_post)
        db.session.commit()

    @staticmethod
    def delete_post(post_id):
        post = PostModel.query.filter(PostModel.id == post_id).first()
        if post is not None:
            db.session.delete(post)
            db.session.commit()
            return True
        else:
            return False

    @staticmethod
    def update_post(post_id, title, body):
        post = PostModel.query.filter(PostModel.id == post_id).first()
        if post is not None:
            post.title = title
            post.body = body
            db.session.add(post)
            db.session.commit()
            return True
        else:
            return False

    @staticmethod
    def search_posts(search_term):
        posts = PostModel.query.filter(
            PostModel.title.like('%' + search_term + '%')
        ).all()
        return posts
