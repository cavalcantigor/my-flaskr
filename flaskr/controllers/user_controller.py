from ..models.user_model import UserModel
from ..models.db import db
from werkzeug.security import generate_password_hash


class UserController:

    @staticmethod
    def get_users(user_id=None):
        if user_id is not None:
            return UserModel.query.all()
        else:
            return UserModel.query.filter(UserModel.id == user_id).first()

    @staticmethod
    def create_user(username, password):
        new_user = UserModel(username=username,
                             password=generate_password_hash(password))
        db.session.add(new_user)
        db.session.commit()

    @staticmethod
    def delete_user(user_id):
        user = UserModel.query.filter(UserModel.id == user_id).first()
        db.session.delete(user)
        db.session.commit()

    @staticmethod
    def search_user(username):
        user = UserModel.query.filter(UserModel.username == username).first()
        return user
