from ..models.comment_model import CommentModel
from ..models.db import db
from datetime import datetime


class CommentController:

    @staticmethod
    def get_comments(comment_id=None):
        if comment_id is not None:
            return CommentModel.query.all()
        else:
            return CommentModel.query.filter(
                CommentModel.id == comment_id
            ).first()

    @staticmethod
    def get_comments_post(post_id):
        return CommentModel.query.filter(CommentModel.post_id == post_id).all()

    @staticmethod
    def create_comment(comment, author_id, post_id):
        new_comment = CommentModel(text_comment=comment, post_id=post_id,
                                   user_id=author_id, created=datetime.now())
        if new_comment is not None:
            db.session.add(new_comment)
            db.session.commit()
            return True
        else:
            return False

    @staticmethod
    def delete_comment(post_id):
        post = CommentModel.query.filter(CommentModel.id == post_id).first()
        if post is not None:
            db.session.delete(post)
            db.session.commit()
            return True
        else:
            return False
