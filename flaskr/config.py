import os


class Config(object):
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = \
        'postgresql://postgres:root@{dbpostgres}/flaskr'.format(
            dbpostgres=os.environ.get('DATABASE_URL', '')
        )


class DevConfig(Config):
    SECRET_KEY = 'dev'


class HerokuConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', '')
    SECRET_KEY = '123heroku!@#'
