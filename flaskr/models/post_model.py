from .db import db


class PostModel(db.Model):
    __tablename__ = 'post'
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    author_id = db.Column('author_id', db.Integer, db.ForeignKey('user.id'))
    created = db.Column('created', db.DateTime, nullable=False)
    title = db.Column('title', db.Text, nullable=False)
    body = db.Column('body', db.Text, nullable=False)
    comments = db.relationship('CommentModel', backref='post')
