from .db import db


class UserModel(db.Model):
    __tablename__ = 'user'
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    username = db.Column('username', db.String, nullable=False, unique=True)
    password = db.Column('password', db.String, nullable=False, unique=True)
    posts = db.relationship('PostModel', backref='user')
    comments = db.relationship('CommentModel', backref='user')
