from .db import db


class CommentModel(db.Model):
    __tablename__ = 'comment'
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column('user_id', db.Integer, db.ForeignKey('user.id'))
    post_id = db.Column('post_id', db.Integer, db.ForeignKey('post.id'))
    text_comment = db.Column('text_comment', db.Text, nullable=False)
    created = db.Column('created', db.DateTime, nullable=False)
