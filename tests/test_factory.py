from flaskr import create_app


def test_config():
    assert not create_app().testing
    assert create_app({
        'TESTING': True,
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'SQLALCHEMY_DATABASE_URI': '',
    }).testing
