import pytest
from flaskr import create_app
from flaskr.models.user_model import UserModel
from unittest.mock import patch
from werkzeug.security import generate_password_hash


class AuthActions(object):
    def __init__(self, client):
        self._client = client

    @patch('flaskr.auth.UserController')
    def register(self, UserMock=None, username='test', password='test'):

        UserMock.search_user.return_value = None
        UserMock.create_user.return_value = {
            'username': 'test',
            'password': 'test'
        }

        return self._client.post(
            '/auth/register',
            data={'username': username, 'password': password}
        )

    @patch('flaskr.auth.UserController')
    def login(self, UserMock, username='test', password='test'):

        UserMock.search_user.return_value = UserModel(
            id=1,
            username=username,
            password=generate_password_hash(password)
        )

        return self._client.post(
            '/auth/login',
            data={'username': username, 'password': password}
        )

    def logout(self):
        return self._client.get('/auth/logout')


@pytest.fixture
def app():

    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'SQLALCHEMY_DATABASE_URI': '',
        'SECRET_KEY': '123abc!@#',
    })

    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def auth(client):
    return AuthActions(client)
