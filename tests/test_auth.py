def test_register(client, auth):
    assert client.get('/auth/register').status_code == 200
    assert auth.register().status_code == 302


def test_login(client, auth):
    assert client.get('/auth/login').status_code == 200
    response = auth.login()
    assert response.headers['Location'] == 'http://localhost/'
